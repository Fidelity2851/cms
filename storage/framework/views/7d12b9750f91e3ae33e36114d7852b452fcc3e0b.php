<!--dashboard bar-->


<!--title-->
<?php $__env->startSection('title'); ?>
    Category - Edit
<?php $__env->stopSection(); ?>

<!--display-->
<?php $__env->startSection('content'); ?>
    <div class="col-10 dis_con pos-absolute p-0">
        <div class="col dis_head d-flex flex-justify-between px-4">
            <p class="dis_header flex-self-center">edit Sub category</p>
            <div class="d-flex flex-self-center">
                <a href="<?php echo e(route('category.list')); ?>" class="no-decor mr-3"> <p class="dis_bind_act">category</p> </a>
                <p class="dis_bind_act">Edit sub-category</p>
            </div>
        </div>


        <div class="post_con p-4">
            <?php if(session()->has('msg')): ?>
                <div class="msg_con">
                    <div class="msg d-flex flex-justify-between flex-self-start">
                        <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                        <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> <?php echo e(session()->get('msg')); ?></p>
                        <button type="button" class="msg_btn flex-self-center">&times;</button>
                    </div>
                </div>
            <?php endif; ?>


            <ul data-role="tabs" data-expand="true">
                <li><a href="#_target_1" class="tab_link">Edit Sub-Category</a></li>
            </ul>
            <div class="border bd-default no-border-top p-2">
                <div id="_target_1">
                    <div class="d-flex flex-justify-between">
                        <div class="col-5 ">
                            <?php $__currentLoopData = $query; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $query): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <form class="post_form" action="<?php echo e(route('subcategory.update', ['subcategory' => $query->id])); ?>" method="post" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <div class="col ">
                                    <label class="post_label">Sub Category Name</label>
                                    <input type="text" value="<?php echo e($query->name); ?>" name="subcategory" class="post_box <?php $__errorArgs = ['subcategory'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input" required>
                                    <?php $__errorArgs = ['subcategory'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <p class="error_msg"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>
                                <div class="col ">
                                    <label class="post_label">Description</label>
                                    <textarea name="description" class="post_area <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="textarea" required><?php echo e($query->description); ?></textarea>
                                    <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <p class="error_msg"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>
                                <div class="col ">
                                    <button type="submit" class="post_btn2">Update</button>
                                </div>
                            </form>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\CMS\resources\views/editsubcategory.blade.php ENDPATH**/ ?>