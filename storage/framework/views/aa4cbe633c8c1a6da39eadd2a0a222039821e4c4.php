<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CMS - Login</title>

    <!--css-->
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/metro.css">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Buda:300|Muli:300,400,500,600,700|Raleway:300,400,500,600,700&display=swap" rel="stylesheet">

    <!--fonts icon-->
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

</head>
<body>
<!--housing-->
<div class="house1 d-flex flex-justify-center">
    <?php if(session()->has('msg')): ?>
        <div class="msg_con mt-5">
            <div class="msg d-flex flex-justify-between flex-self-start">
                <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> <?php echo e(session()->get('msg')); ?></p>
                <button type="button" class="msg_btn flex-self-center">&times;</button>
            </div>
        </div>
    <?php endif; ?>

    <div class="d-flex flex-justify-center flex-self-center px-10">
        <!--login-->
        <div class="log_con col-3 flex-self-center p-10 mr-10">
            <p class="form_header">login</p>
            <form class="log_form" action="/dashboard" method="post" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="mb-4">
                    <label class="log_label">Email / Username</label>
                    <input type="text" class="log_box" data-role="input">
                </div>
                <div class="mb-4">
                    <label class="log_label">Password</label>
                    <input type="password" class="log_box" data-role="input">
                </div>
                <div class="d-flex flex-justify-center">
                    <button type="submit" class="log_btn" data-role="password">Login</button>
                </div>
            </form>
        </div>
        <p class="log_header flex-self-center">Content Management System</p>
    </div>

</div>

</body>
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/metro.js"></script>
<script type="text/javascript" src="js/index.js"></script>
</html>
<?php /**PATH C:\laragon\www\CMS\resources\views/index.blade.php ENDPATH**/ ?>