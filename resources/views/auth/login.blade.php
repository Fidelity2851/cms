{{--@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection--}}

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CMS - Login</title>

        <!--css-->
        <link rel="stylesheet" href="css/index.css">
        <link rel="stylesheet" href="css/metro.css">

        <!--google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Buda:300|Muli:300,400,500,600,700|Raleway:300,400,500,600,700&display=swap" rel="stylesheet">

        <!--fonts icon-->
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    </head>
    <body>
    <!--housing-->
    <div class="house1 d-flex flex-justify-center">
        @if(session()->has('msg'))
            <div class="msg_con mt-5">
                <div class="msg d-flex flex-justify-between flex-self-start">
                    <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                    <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                    <button type="button" class="msg_btn flex-self-center">&times;</button>
                </div>
            </div>
        @endif

        <div class="d-flex flex-justify-center flex-self-center px-10">
            <!--login-->
            <div class="log_con col-3 flex-self-center p-10 mr-10">
                <p class="form_header">login</p>
                <form class="log_form" method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="mb-4">
                        <label class="log_label">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" name="email" class="log_box @error('email') is-invalid @enderror" value="{{ old('email') }}" data-role="input" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback log_label" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <label class="log_label">{{ __('Password') }}</label>
                        <input id="password" type="password" class="log_box @error('password') is-invalid @enderror" data-role="input" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="log_label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                    <div class="d-flex flex-column">
                        <button type="submit" class="log_btn mb-3" data-role="password"> {{ __('Login') }} </button>
                        @if (Route::has('password.request'))
                            <a class="" href="{{ route('password.request') }}">
                                <p class="log_label text-center">{{ __('Forgot Your Password?') }}</p>
                            </a>
                        @endif
                    </div>
                </form>
            </div>
            <p class="log_header flex-self-center">Content Management System</p>
        </div>

    </div>

    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/metro.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    </body>

</html>
