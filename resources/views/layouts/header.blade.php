<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">--}}

    <title>CMS - @yield('title')</title>

    <!--css-->
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
    <link rel="stylesheet" href="{{ asset('css/metro-all.css') }}">


    {{--wiziwig CDN--}}
    <script src="https://cdn.ckeditor.com/ckeditor5/19.1.1/classic/ckeditor.js"></script>

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Buda:300|Muli:300,400,500,600,700|Raleway:300,400,500,600,700&display=swap" rel="stylesheet">

    <!--fonts icon-->
{{--
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
--}}

</head>
<body>
<!--housing-->
<div  class="house d-flex flex-justify-between">

    <!--dashboard bar-->
    <div class="col-2 dash_con pos-fixed p-0">
        <div class="col px-4">
            <p class="dis_header text-center">cms</p>
        </div>
        <div class="col dash_img_con d-flex py-4">
            @if(Auth::check())
                @if(Auth::user()->image)
                    <img class="dash_img flex-align-center mr-3" src="{{ asset('../storage/'. Auth::user()->image) }}" alt="User Image"/>
                @else
                    <img class="dash_img flex-align-center mr-3" src="{{ asset("../images/user_icon.svg") }}" alt="Default Image"/>
                @endif
                <div class="flex-self-center">
                    <p class="dash_img_name">{{ Auth::user()->username }}</p>
                    <div class="d-flex">
                        <p class="log_role flex-self-center">{{ Auth::user()->role }}</p>
                        <a class="dropdown-item no-decor flex-self-center ml-3" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                            <span class="log">{{ __('Logout') }}</span>
                        </a>
                    </div>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            @endif
        </div>
        <div class="dash_link_con">
            <a href="{{ route('dashboard') }}" class="no-decor"> <p class="@if(isset($dash)) dash_link_act  @else dash_link @endif">dashboard</p> </a>
            @can('viewAny', App\category::class)
                <a href="{{ route('category.list') }}" class="no-decor"> <p class="@if(isset($cate)) dash_link_act  @else dash_link @endif">category</p> </a>
            @endcan
            @can('viewAny', App\post::class)
                <a href="{{ route('post.list') }}" class="no-decor"> <p class="@if(isset($post)) dash_link_act  @else dash_link @endif">post</p> </a>
            @endcan
            @can('viewAny', App\banner::class)
                <a href="{{ route('banner.list') }}" class="no-decor"> <p class="@if(isset($ban)) dash_link_act  @else dash_link @endif">banner</p> </a>
            @endcan
            @can('viewAny', App\media::class)
                <a href="{{ route('media.list') }}" class="no-decor"> <p class="@if(isset($med)) dash_link_act  @else dash_link @endif">multimedia</p> </a>
            @endcan
            @can('viewAny', App\faq::class)
                <a href="{{ route('faq.list') }}" class="no-decor"> <p class="@if(isset($faq)) dash_link_act  @else dash_link @endif">faq</p> </a>
            @endcan
            @can('viewAny', App\User::class)
                <a href="{{ route('users.list') }}" class="no-decor"> <p class="@if(isset($user)) dash_link_act  @else dash_link @endif">users</p> </a>
            @endcan
            @can('viewAny', App\setting::class)
                <a href="{{ route('setting.list') }}" class="no-decor"> <p class="@if(isset($set)) dash_link_act  @else dash_link @endif">setting</p> </a>
            @endcan
        </div>
    </div>

    @yield('content')


</div>

{{--wiziwig script--}}
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
            console.log( editor );
        } )
        .catch( error => {
            console.error( error );
        } );

</script>
{{--<!--vue js-->
<script src="{{ asset('js/app.js') }}" defer></script>--}}

{{--Javascript files--}}
<script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/metro.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/index.js') }}"></script>
</body>
</html>
