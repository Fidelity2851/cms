<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Faq
@endsection

<!--display-->
@section('content')
        <div class="col-10 dis_con pos-absolute p-0">
            <div class="col dis_head d-flex flex-justify-between px-4">
                <p class="dis_header flex-self-center">Faq</p>
                <div class="flex-self-center">
                    <p class="dis_bind_act">Faq</p>
                </div>
            </div>

            <div class="post_con p-4">
                @if(session()->has('msg'))
                    <div class="msg_con">
                        <div class="msg d-flex flex-justify-between flex-self-start">
                            <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                            <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                            <button type="button" class="msg_btn flex-self-center">&times;</button>
                        </div>
                    </div>
                @endif
                <ul data-role="tabs" data-expand="true">
                    <li><a href="#_target_1" class="tab_link">Create Faq</a></li>
                    <li><a href="#_target_2" class="tab_link">Manger Faq</a></li>
                </ul>
                <div class="border bd-default no-border-top p-2">
                    <div id="_target_1">
                        <div class="d-flex flex-justify-between">
                            <div class="col p-0">
                                <form class="post_form" action="{{ route('faq.store') }}" method="post">
                                    @csrf
                                    <div class="col ">
                                        <label class="post_label">Question</label>
                                        <input type="text" value="{{old('question')}}" name="question" class="post_box  @error('question') is-invalid @enderror" data-role="input" required>
                                        @error('question')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Answer</label>
                                        <textarea name="answer" class="post_area @error('answer') is-invalid @enderror" data-role="textarea" required>{{old('answer')}}</textarea>
                                        @error('answer')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                        <button type="submit" class="post_btn2">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="_target_2">
                        <form method="post" action="{{ route('faq.deletemany') }}">
                            @csrf
                            <table class="table table-border row-border compact striped"
                                   data-check="false"
                                   data-show-search="false"
                                   data-show-rows-steps="false"
                                   data-horizontal-scroll="false"
                                   data-show-pagination="false"
                            >
                                <thead>
                                <tr>
                                    <th class="t_head" > <input type="checkbox" name="check_all" data-role="checkbox"> </th>
                                    <th class="t_head" data-sortable="true">ID</th>
                                    <th class="t_head" data-sortable="true">Question</th>
                                    <th class="t_head" data-sortable="true">Answers</th>
                                    <th class="t_head" data-sortable="true">Created date</th>
                                    <th class="t_head">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($faqs as $faq)
                                    <tr>
                                        <td class="t_data"><input value="{{$faq->id}}" name="checked[]" type="checkbox" data-role="checkbox"></td>
                                        <td>{{$faq->id}}</td>
                                        <td>{{$faq->questions}}</td>
                                        <td>{{$faq->answers}}</td>
                                        <td>{{$faq->created_at}}</td>
                                        <td> <a href="{{ route('faq.fetch', ['faq' => $faq->id]) }}" title="Edit" class="mif-open-book t_icon"></a> <a href="{{ route('faq.delete', ['faq' => $faq->id]) }}" title="Delete" class="mif-bin t_icon"></a> </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="">
                                <button type="submit" class="del_all_btn mb-3">Delete</button>
                                {{ $faqs->links() }}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
