<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Category
@endsection

<!--display-->
@section('content')
    <div class="col-10 dis_con pos-absolute p-0">
        <div class="col dis_head d-flex flex-justify-between px-4">
            <p class="dis_header flex-self-center">category</p>
            <div class="flex-self-center">
                <p class="dis_bind_act">category</p>
            </div>
        </div>

        <div class="post_con p-4">
            @if(session()->has('msg'))
            <div class="msg_con">
                <div class="msg d-flex flex-justify-between flex-self-start">
                    <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                    <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                    <button type="button" class="msg_btn flex-self-center">&times;</button>
                </div>
            </div>
            @endif

            <ul class="flex-self-center" data-role="tabs" data-expand="true">
                <li><a href="#_target_1" class="tab_link">Create Category</a></li>
                <li><a href="#_target_2" class="tab_link">Manger Category</a></li>
            </ul>

            <div class="border bd-default no-border-top p-2">
                <div id="_target_1">
                    <div class="d-flex flex-justify-between">
                        <div class="col-5 ">
                            <form class="post_form" action="{{ route('category.store') }}" method="post" >
                                @csrf
                                <div class="col ">
                                    <label class="post_label">Category Name</label>
                                    <input type="text" value="{{old('category_name')}}" name="category_name" class="post_box @error('category_name') is-invalid @enderror" data-role="input" required>
                                    @error('category_name')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Description</label>
                                    <textarea name="description" class="post_area @error('description') is-invalid @enderror" data-role="textarea" required>{{old('description')}}</textarea>
                                    @error('description')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                    <button type="submit" class="post_btn2">Submit</button>
                                </div>

                            </form>
                        </div>
                        <div class="col-5 ">
                            <form class="post_form" action="{{ route('subcategory.store') }}" method="post">
                                @csrf
                                <div class="col ">
                                    <label class="post_label">Category Name</label>
                                    <select name="category" class="post_sel @error('category') is-invalid @enderror" data-role="select" required>
                                        <option disabled selected>select a category</option>
                                        @foreach($query as $data)
                                            <option value="{{$data->id}}">{{$data->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('category')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Sub-category Name</label>
                                    <input type="text" name="subcategory" value="{{old('subcategory')}}" class="post_box @error('subcategory') is-invalid @enderror" data-role="input" required>
                                    @error('subcategory')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Description</label>
                                    <textarea name="subdescription" class="post_area @error('subdescription') is-invalid @enderror" data-role="textarea" required>{{old('subdescription')}}</textarea>
                                    @error('subdescription')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                    <button type="submit" class="post_btn2">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="_target_2">
                    <form method="post" action="{{ route('category.deletemany') }}">
                        @csrf
                        <table class="table table-border row-border compact striped"
                               data-check="false"
                               data-show-search="false"
                               data-show-rows-steps="false"
                               data-horizontal-scroll="false"
                               data-show-pagination="false"
                        >
                            <thead>
                            <tr>
                                <th class="t_head" > <input name="check_all" type="checkbox" data-role="checkbox"> </th>
                                <th class="t_head sortable-column" >ID</th>
                                <th class="t_head sortable-column" >Category</th>
                                <th class="t_head sortable-column" >Description</th>
                                <th class="t_head sortable-column" >Created date</th>
                                <th class="t_head">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($query as $data)
                                <tr>
                                    <td class="t_data"><input value="{{$data->id}}" name="checked[]" type="checkbox" data-role="checkbox"></td>
                                    <td class="t_data">{{$data->id}}</td>
                                    <td class="t_data">{{$data->name}}</td>
                                    <td class="t_data">{{$data->description}}</td>
                                    <td class="t_data">{{$data->created_at}}</td>
                                    <td class="t_data">
                                        <a href="{{ route('category.fetch', ['category' => $data->id]) }}" title="Edit" class="mif-open-book t_icon"></a>
                                        <a title="Delete" href="{{ route('category.delete', ['category' => $data->id]) }}" class="mif-bin t_icon con_del"> </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="">
                            <button type="submit" class="del_all_btn mb-3">Delete</button>
                            {{ $query->links() }}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
