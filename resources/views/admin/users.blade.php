<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Users
@endsection

<!--display-->
@section('content')
    <div class="col-10 dis_con pos-absolute p-0">
        <div class="col dis_head d-flex flex-justify-between px-4">
            <p class="dis_header flex-self-center">users</p>
            <div class="flex-self-center">
                <p class="dis_bind_act">users</p>
            </div>
        </div>

        <div class="post_con p-4">
            @if(session()->has('msg'))
                <div class="msg_con">
                    <div class="msg d-flex flex-justify-between flex-self-start">
                        <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                        <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                        <button type="button" class="msg_btn flex-self-center">&times;</button>
                    </div>
                </div>
            @endif
            <ul data-role="tabs" data-expand="true">
                @can('create', App\User::class)
                    <li><a href="#_target_1" class="tab_link">Create Users</a></li>
                @endcan
                <li><a href="#_target_2" class="tab_link">Manger Users</a></li>
            </ul>
            <div class="border bd-default no-border-top p-2">
                @can('create', App\User::class)
                    <div id="_target_1">
                    <div class="d-flex flex-justify-between">
                        <div class="col p-0">
                            <form class="post_form" action="{{ route('users.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="d-flex flex-justify-between">
                                    <div class="col ">
                                        <label class="post_label">Username</label>
                                        <input type="text" value="{{old('username')}}" name="username" class="post_box  @error('username') is-invalid @enderror" data-role="input" required>
                                        @error('username')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Email Address</label>
                                        <input type="Email" value="{{old('email')}}" name="email" class="post_box  @error('email') is-invalid @enderror" data-role="input" required>
                                        @error('email')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">User Role</label>
                                        <select name="role" class="post_sel  @error('role') is-invalid @enderror" data-role="select" required>
                                            <option value="user">User</option>
                                            <option value="writter">Writter</option>
                                            <option value="admin">Admin</option>
                                        </select>
                                        @error('role')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="d-flex flex-justify-between">
                                    <div class="col ">
                                        <label class="post_label">Profile photo</label>
                                        <input type="file" name="image" class="post_box  @error('image') is-invalid @enderror" data-role="file" required>
                                        @error('image')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Password</label>
                                        <input type="password" name="password" class="post_box  @error('password') is-invalid @enderror" data-role="input" required>
                                        @error('password')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Comfirm Password</label>
                                        <input id="password-confirm" type="password" name="comfirm_password" class="post_box  @error('comfirm_password') is-invalid @enderror" data-role="input" required>
                                        @error('comfirm_password')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col ">
                                    <label class="post_label">Short Biograph</label>
                                    <textarea name="biograph" class="post_area @error('biograph') is-invalid @enderror" data-role="textarea" required>{{old('biograph')}}</textarea>
                                    @error('biograph')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                    <button type="submit" class="post_btn2">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endcan
                <div id="_target_2">
                    <form method="post" action="{{ route('users.deletemany') }}">
                        @csrf
                        <table class="table table-border row-border compact striped"
                               data-check="false"
                               data-show-search="false"
                               data-show-rows-steps="false"
                               data-horizontal-scroll="false"
                               data-show-pagination="false"
                        >
                            <thead>
                            <tr>
                                <th class="t_head" > <input type="checkbox" name="check_all" data-role="checkbox"> </th>
                                <th class="t_head" data-sortable="true">ID</th>
                                <th class="t_head" data-sortable="true">Username</th>
                                <th class="t_head" data-sortable="true">Biograph</th>
                                <th class="t_head" data-sortable="true">Email</th>
                                <th class="t_head" data-sortable="true">Role</th>
                                <th class="t_head" data-sortable="true">Status</th>
                                <th class="t_head" data-sortable="true">Registered date</th>
                                <th class="t_head">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td class="t_data"><input value="{{$user->id}}" name="checked[]" type="checkbox" data-role="checkbox"></td>
                                    <td class="t_data">{{$user->id}}</td>
                                    <td class="t_data"><img class="dash_img mr-3" src="storage/{{$user->image}}"> {{$user->username}}</td>
                                    <td class="t_data">{{$user->biograph}}</td>
                                    <td class="t_data">{{$user->email}}</td>
                                    <td class="t_data">
                                        <span class="data_w">{{$user->role}}</span>
                                    </td>
                                    <td class="t_data">
                                        @if($user->status==1)
                                            <span class="data_g">Active</span>
                                        @elseif($user->status==0)
                                            <span class="data_r">Blocked</span>
                                        @endif
                                    </td>
                                    <td class="t_data">{{$user->created_at}}</td>
                                    <td>
                                        @if($user->status==0)
                                            @can('create', App\User::class)
                                                <a href="{{ route('users.unblock', ['user' => $user->id]) }}" title="Unblock" class="mif-done t_icon unblo_btn"></a>
                                            @endcan
                                        @elseif($user->status==1)
                                            @can('create', App\User::class)
                                                <a href="{{ route('users.block', ['user' => $user->id]) }}" title="Block" class="mif-lock t_icon blo_btn"></a>
                                            @endcan
                                        @endif
                                        <a href="{{ route('users.fetch', ['user' => $user->id]) }}" title="Edit" class="mif-open-book t_icon">

                                        </a> <a href="{{ route('users.delete', ['user' => $user->id]) }}" title="Delete" class="mif-bin t_icon con_del"></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="">
                            <button id="del_btn" type="submit" class="del_all_btn mb-3">Delete</button>
                            {{ $users->links() }}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
