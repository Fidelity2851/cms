<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Media - Edit
@endsection

<!--display-->
@section('content')
    <div class="col-10 dis_con pos-absolute p-0">
        <div class="col dis_head d-flex flex-justify-between px-4">
            <p class="dis_header flex-self-center">edit media</p>
            <div class="flex-self-center d-flex">
                <a href="{{ route('media.list') }}" class="no-decor mr-3"> <p class="dis_bind_act">Media</p> </a>
                <p class="dis_bind_act">Editmedia</p>
            </div>
        </div>


        <div class="post_con p-4">
            @if(session()->has('msg'))
                <div class="msg_con">
                    <div class="msg d-flex flex-justify-between flex-self-start">
                        <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                        <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                        <button type="button" class="msg_btn flex-self-center">&times;</button>
                    </div>
                </div>
            @endif

            <ul data-role="tabs" data-expand="true">
                <li><a href="#_target_1" class="tab_link">Edit Media</a></li>
            </ul>
            <div class="border bd-default no-border-top p-2">
                <div id="_target_1">
                    <div class="d-flex flex-justify-between">
                        <div class="col-5 p-0">
                            <form class="post_form" action="{{ route('media.update', ['media' => $query->id]) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="col ">
                                    <label class="post_label">Media Title</label>
                                    <input type="text" value="{{$query->title}}" name="title" class="post_box @error('title') is-invalid @enderror" data-role="input" required>
                                    @error('title')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Cover Image</label>
                                    <input type="file" name="image" class="post_box @error('image') is-invalid @enderror" data-role="file" >
                                    @error('image')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                    <div class="p-3">
                                        <img class="post_img" src="{{asset('storage/'.$query->image)}}">
                                    </div>
                                </div>
                                <div class="col ">
                                    <label class="post_label">Description</label>
                                    <textarea name="description" class="post_area @error('description') is-invalid @enderror" data-role="textarea" required>{{$query->description}}</textarea>
                                    @error('description')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <button type="submit" class="post_btn2">Update</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-5 p-0">
                            <form class="post_form" action="{{ route('gallery.update', ['gallery' => $query->id]) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="col ">
                                    <label class="post_label">Media Title</label>
                                    <select name="media_title" class="post_sel @error('media_title') is-invalid @enderror" data-role="select" required>
                                        <option value="{{$query->id}}">{{$query->title}}</option>
                                    </select>
                                    @error('media_title')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Photos</label>
                                    <input type="file" name="photos" class="post_box @error('photos') is-invalid @enderror" data-role="file" required>
                                    @error('photos')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                    <div class="col mt-2 px-0">
                                        <button type="submit" class="post_btn2">Update</button>
                                    </div>
                                    <div class="row py-3 mx-0">
                                        @foreach($query->gallery as $data)
                                            <div class="pos-relative mx-auto">
                                                <a href="{{ route('gallery.delete', ['gallery' => $data->id]) }}" class="del_link no-decor pos-absolute con_del"> <button type="button" class="del_btn">&times;</button> </a>
                                                <img class="post_img" src="{{asset('storage/'.$data->name)}}">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
