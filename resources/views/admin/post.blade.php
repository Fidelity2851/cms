<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Post
@endsection

<!--display-->
@section('content')
        <div class="col-10 dis_con pos-absolute p-0">
            <div class="col dis_head d-flex flex-justify-between px-4">
                <p class="dis_header flex-self-center">post</p>
                <div class="flex-self-center">
                    <p class="dis_bind_act">post</p>
                </div>
            </div>


            <div class="post_con p-4">

                @if(session()->has('msg'))
                    <div class="msg_con">
                        <div class="msg d-flex flex-justify-between flex-self-start">
                            <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                            <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                            <button type="button" class="msg_btn flex-self-center">&times;</button>
                        </div>
                    </div>
                @endif

                <ul data-role="tabs" data-expand="true">
                    <li><a href="#_target_1" class="tab_link">Create Post</a></li>
                    <li><a href="#_target_2" class="tab_link">Manger Post</a></li>
                </ul>
                <div class="border bd-default no-border-top p-2">
                    <div id="_target_1">
                        <div class="d-flex flex-justify-between">
                                <div class="col p-0">
                                    <form class="post_form" action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="d-flex flex-justify-between">
                                            <div class="col ">
                                                <label class="post_label">Post Title</label>
                                                <input type="text" value="{{old('title')}}" name="title" class="post_box @error('title') is-invalid @enderror" data-role="input" required>
                                                @error('title')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col ">
                                                <label class="post_label">Slug/URl (Optional)</label>
                                                <input type="text" value="{{old('slug')}}" name="slug" class="post_box @error('slug') is-invalid @enderror" data-role="input" >
                                                @error('slug')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="d-flex flex-justify-between">
                                            <div class="col ">
                                                <label class="post_label">Post image</label>
                                                <input type="file" value="{{old('image')}}" name="image" class="post_box @error('image') is-invalid @enderror" data-role="file" required>
                                                @error('image')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col-3 ">
                                                <label class="post_label">Category</label>
                                                <select id="category" name="category" class="post_sel cate @error('category') is-invalid @enderror" data-role="select" required>
                                                    <option disabled selected>select a category</option>
                                                    @foreach($query1 as $data1)
                                                        <option value="{{$data1->id}}">{{$data1->name}}</option>
                                                    @endforeach
                                                </select>
                                                @error('category')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col-3 ">
                                                <label class="post_label">Sub-category</label>
                                                <select name="subcategory" class="post_sel sub_cate @error('subcategory') is-invalid @enderror" required>
                                                    {{--@foreach($query2 as $data2)
                                                        <option value="{{$data2->id}}">{{$data2->name}}</option>
                                                    @endforeach--}}
                                                </select>
                                                <span class="feedback">Select a Category first</span>
                                                @error('subcategory')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="d-flex flex-justify-between">
                                            <div class="col-6 ">
                                                <label class="post_label">Summary</label>
                                                <textarea name="summary" class="post_area @error('summary') is-invalid @enderror" data-role="textarea">{{old('summary')}}</textarea>
                                                @error('summary')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col ">
                                                <label class="post_label">Tags</label>
                                                <input type="text" value="{{old('tags')}}" name="tags" class="post_box @error('tags') is-invalid @enderror" data-role="taginput" required>
                                                @error('tags')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col ">
                                                <label class="post_label">Publision Date</label>
                                                <input type="text" name="pub_date" class="post_box @error('pub_date') is-invalid @enderror" data-role="calendarpicker" data-dialog-mode="true" value="<?php echo now(); ?>">
                                                @error('pub_date')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col ">
                                            <label class="post_label">Description</label>
                                            <textarea id="editor" name="description" class="post_area @error('description') is-invalid @enderror">{{old('description')}}</textarea>
                                            @error('description')
                                            <p class="error_msg">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="col ">
                                            <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                            <button type="submit" class="post_btn2">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                    </div>
                    <div id="_target_2">
                        <form method="post" action="{{ route('post.deletemany') }}">
                            @csrf
                            <div class="d-flex flex-justify-end">
                                <a href="{{ route('post.list') }}" class="mr-3"><button type="button" class="data_g">All Post</button></a>
                                <a href="{{ route('post.listapprove') }}" class="mr-3"><button type="button" class="data_g">Approve Post</button></a>
                                <a href="{{ route('post.listpending') }}" class=""><button type="button" class="data_g">Pending Post</button></a>
                            </div>
                            <table class="table table-border row-border compact striped"
                                   data-check="false"
                                   data-show-search="false"
                                   data-show-rows-steps="false"
                                   data-horizontal-scroll="false"
                                   data-show-pagination="false"
                            >
                                <thead>
                                <tr>
                                    <th class="t_head" > <input id="check_all" name="check_all" type="checkbox" data-role="checkbox"> </th>
                                    <th class="t_head ">ID</th>
                                    <th class="t_head ">Title</th>
                                    <th class="t_head ">Category</th>
                                    <th class="t_head ">Author</th>
                                    <th class="t_head ">Status</th>
                                    <th class="t_head ">Published date</th>
                                    <th class="t_head ">Created date</th>
                                    <th class="t_head">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($query as $datas)
                                    <tr>
                                        <td class="t_data"><input value="{{$datas->id}}" name="checked[]" type="checkbox" data-role="checkbox"></td>
                                        <td class="t_data">{{$datas->id}}</td>
                                        <td class="t_data">{{$datas->title}}</td>
                                        <td class="t_data">{{$datas->category->name}}</td>
                                        <td class="t_data">{{$datas->user->username}}</td>
                                        <td class="t_data">
                                            @if($datas->status==1)
                                                <span class="data_g">Approved</span>
                                            @elseif($datas->status==0)
                                                <span class="data_w">Pending</span>
                                            @endif
                                        </td>
                                        <td class="t_data">{{$datas->pub_at}}</td>
                                        <td class="t_data">{{$datas->created_at}}</td>
                                        <td class="t_data">
                                            @if($datas->status==0)
                                                @can('approve', App\post::class)
                                                    <a href="{{ route('post.approve', ['post' => $datas->id]) }}" title="Approve" class="mif-done t_icon"></a>
                                                @endcan
                                            @elseif($datas->status==1)
                                                @can('approve', App\post::class)
                                                    <a href="{{ route('post.pending', ['post' => $datas->id]) }}" title="Pending" class="mif-lock t_icon"></a>
                                                @endcan
                                            @endif
                                            <a href="{{ route('post.fetch', ['post' => $datas->id]) }}" title="Edit" class="mif-open-book t_icon"></a>
                                            <a href="{{ route('post.delete', ['post' => $datas->id]) }}" title="Delete" class="mif-bin t_icon con_del"></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="">
                                <button type="submit" class="del_all_btn mb-3">Delete</button>
                                {{ $query->links() }}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
