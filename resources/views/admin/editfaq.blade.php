<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Faq - Edit
@endsection

<!--display-->
@section('content')
    <div class="col-10 dis_con pos-absolute p-0">
        <div class="col dis_head d-flex flex-justify-between px-4">
            <p class="dis_header flex-self-center">Faq</p>
            <div class="flex-self-center d-flex">
                <a href="{{ route('faq.list') }}" class="no-decor mr-3"><p class="dis_bind_act">Faq</p></a>
                <p class="dis_bind_act">EditFaq</p>
            </div>
        </div>

        <div class="post_con p-4">
            @if(session()->has('msg'))
                <div class="msg_con">
                    <div class="msg d-flex flex-justify-between flex-self-start">
                        <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                        <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                        <button type="button" class="msg_btn flex-self-center">&times;</button>
                    </div>
                </div>
            @endif
            <ul data-role="tabs" data-expand="true">
                <li><a href="#_target_1" class="tab_link">Edit Faq</a></li>
            </ul>
            <div class="border bd-default no-border-top p-2">
                <div id="_target_1">
                    <div class="d-flex flex-justify-between">
                        <div class="col p-0">
                            <form class="post_form" action="{{ route('faq.update', ['faq' => $query->id]) }}" method="post">
                                @csrf
                                <div class="col ">
                                    <label class="post_label">Question</label>
                                    <input type="text" value="{{$query->questions}}" name="question" class="post_box  @error('question') is-invalid @enderror" data-role="input" required>
                                    @error('question')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Answer</label>
                                    <textarea name="answer" class="post_area @error('answer') is-invalid @enderror" data-role="textarea" required>{{$query->answers}}</textarea>
                                    @error('answer')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <button type="submit" class="post_btn2">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
