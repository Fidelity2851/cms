<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Settings
@endsection

<!--display-->
@section('content')
        <div class="col-10 dis_con pos-absolute p-0">
            <div class="col dis_head d-flex flex-justify-between px-4">
                <p class="dis_header flex-self-center">settings</p>
                <div class="flex-self-center">
                    <p class="dis_bind_act">settings</p>
                </div>
            </div>

            <div class="post_con p-4">
                @if(session()->has('msg'))
                    <div class="msg_con">
                        <div class="msg d-flex flex-justify-between flex-self-start">
                            <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                            <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                            <button type="button" class="msg_btn flex-self-center">&times;</button>
                        </div>
                    </div>
                @endif
                <div class="d-flex flex-justify-between">
                    <div class="col p-0">
                        <form class="post_form" action="{{ route('setting.update') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="d-flex flex-justify-between">
                                <div class="col ">
                                    <label class="post_label">Facebook URl</label>
                                    <input type="url" value=" {{$setting->facebook}}" name="facebook" class="post_box @error('facebook') is-invalid @enderror" data-role="input">
                                    @error('facebook')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Twitter URl</label>
                                    <input type="url" value="{{$setting->twitter}} " name="twitter" class="post_box @error('twitter') is-invalid @enderror" data-role="input">
                                    @error('twitter')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="d-flex flex-justify-between">
                                <div class="col ">
                                    <label class="post_label">Instagram URl</label>
                                    <input type="url" value="{{$setting->Instagram}} " name="instagram" class="post_box @error('instagram') is-invalid @enderror" data-role="input">
                                    @error('instagram')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Google URl</label>
                                    <input type="url" value=" {{$setting->google}} " name="google" class="post_box @error('google') is-invalid @enderror" data-role="input" >
                                    @error('google')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col ">
                                <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                <button type="submit" class="post_btn2">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
