<!--dashboard bar-->
@extends('layouts.header')


@section('title')
    Dashboard
@endsection

@section('content')
        <!--display-->
        <div class="col-10 dis_con pos-absolute p-0">
            <div class="col dis_head d-flex flex-justify-between px-4">
                <p class="dis_header flex-self-center">dashboard</p>
                <div class="flex-self-center">
                    <p class="dis_bind_act">dashboard</p>
                </div>
            </div>
            <div class="form_con row px-4 mx-0">
                @can('viewAny', App\User::class)
                    <a href="{{ route('post.list') }}" class="col-3 px-0 no-decor mb-5">
                        <div class="col dis_img_con d-flex py-3 px-4">
                            <img class="dis_img mr-4" src="images/post_icon.png">
                            <div class="flex-self-center">
                                <p class="dis_img_name">Total Post</p>
                                <p class="dis_img_num">{{$post_row}}</p>
                            </div>
                        </div>
                    </a>
                @endcan
                @can('viewAny', App\User::class)
                    <a href="{{ route('post.listapprove') }}" class="col-3 px-0 no-decor mb-5">
                        <div class="col dis_img_con d-flex py-3 px-4">
                            <img class="dis_img mr-4" src="images/post_icon.png">
                            <div class="flex-self-center">
                                <p class="dis_img_name">Approved Post</p>
                                <p class="dis_img_num">{{$post_approved_row}}</p>
                            </div>
                        </div>
                    </a>
                @endcan
                    @can('viewAny', App\User::class)
                        <a href="{{ route('post.listpending') }}" class="col-3 px-0 no-decor mb-5">
                            <div class="col dis_img_con d-flex py-3 px-4">
                                <img class="dis_img mr-4" src="images/post_icon.png">
                                <div class="flex-self-center">
                                    <p class="dis_img_name">Pending Post</p>
                                    <p class="dis_img_num">{{$post_pending_row}}</p>
                                </div>
                            </div>
                        </a>
                    @endcan
                @can('create', App\User::class)
                    <a href="{{ route('category.list') }}" class="col-3 px-0 no-decor mb-5">
                    <div class="col dis_img_con d-flex py-3 px-4">
                        <img class="dis_img mr-4" src="images/categories_icon.svg">
                        <div class="flex-self-center">
                            <p class="dis_img_name">category</p>
                            <p class="dis_img_num">{{$cate_row}}</p>
                        </div>
                    </div>
                </a>
                @endcan
                @can('create', App\User::class)
                    <a href="{{ route('users.list') }}" class="col-3 px-0 no-decor mb-5">
                    <div class="col dis_img_con d-flex py-3 px-4">
                        <img class="dis_img mr-4" src="images/user_icon.svg">
                        <div class="flex-self-center">
                            <p class="dis_img_name">users</p>
                            <p class="dis_img_num">{{$user_row}}</p>
                        </div>
                    </div>
                </a>
                @endcan
                @can('create', App\User::class)
                    <a href="{{ route('users.list') }}" class="col-3 px-0 no-decor mb-3">
                    <div class="col dis_img_con d-flex py-3 px-4">
                        <img class="dis_img mr-4" src="images/writter_icon.svg">
                        <div class="flex-self-center">
                            <p class="dis_img_name">writter</p>
                            <p class="dis_img_num">{{$write_row}}</p>
                        </div>
                    </div>
                </a>
                @endcan
                @can('create', App\User::class)
                    <a href="{{ route('banner.list') }}" class="col-3 px-0 no-decor mb-5">
                    <div class="col dis_img_con d-flex py-3 px-4">
                        <img class="dis_img mr-4" src="images/post_icon.png">
                        <div class="flex-self-center">
                            <p class="dis_img_name">Banner</p>
                            <p class="dis_img_num">{{$banner_row}}</p>
                        </div>
                    </div>
                </a>
                @endcan
                @can('create', App\User::class)
                    <a href="{{ route('media.list') }}" class="col-3 px-0 no-decor mb-5">
                    <div class="col dis_img_con d-flex py-3 px-4">
                        <img class="dis_img mr-4" src="images/categories_icon.svg">
                        <div class="flex-self-center">
                            <p class="dis_img_name">Multimedia</p>
                            <p class="dis_img_num">{{$media_row}}</p>
                        </div>
                    </div>
                </a>
                @endcan
                @can('create', App\User::class)
                <a href="{{ route('faq.list') }}" class="col-3 px-0 no-decor mb-3">
                    <div class="col dis_img_con d-flex py-3 px-4">
                        <img class="dis_img mr-4" src="images/writter_icon.svg">
                        <div class="flex-self-center">
                            <p class="dis_img_name">Faq</p>
                            <p class="dis_img_num">{{$faq_row}}</p>
                        </div>
                    </div>
                </a>
                @endcan
            </div>
        </div>

@endsection
