<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Category - Edit
@endsection

<!--display-->
@section('content')
    <div class="col-10 dis_con pos-absolute p-0">
        <div class="col dis_head d-flex flex-justify-between px-4">
            <p class="dis_header flex-self-center">edit category</p>
            <div class="d-flex flex-self-center">
                <a href="{{ route('category.list') }}" class="no-decor mr-3"> <p class="dis_bind_act">category</p> </a>
                <p class="dis_bind_act">Editcategory</p>
            </div>
        </div>


        <div class="post_con p-4">
            @if(session()->has('msg'))
                <div class="msg_con">
                    <div class="msg d-flex flex-justify-between flex-self-start">
                        <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                        <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                        <button type="button" class="msg_btn flex-self-center">&times;</button>
                    </div>
                </div>
            @endif


            <ul data-role="tabs" data-expand="true">
                <li><a href="#_target_1" class="tab_link">Edit Category</a></li>
            </ul>
            <div class="border bd-default no-border-top p-2">
                <div id="_target_1">
                    <div class="d-flex flex-justify-between">
                        <div class="col-5 ">
                            <form class="post_form" action="{{ route('category.update', ['category' => $query->id]) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="col ">
                                    <label class="post_label">Category Name</label>
                                    <input type="text" value="{{$query->name}}" name="category_name" class="post_box @error('category_name') is-invalid @enderror" data-role="input" required>
                                    @error('category_name')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Description</label>
                                    <textarea name="description" class="post_area @error('description') is-invalid @enderror" data-role="textarea" required>{{$query->description}}</textarea>
                                    @error('description')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <button type="submit" class="post_btn2">Update</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-3 ">
                            <p class="post_head text-center">Sub categories</p>
                            @foreach($query->subcategory as $query)
                                <div class="d-flex flex-justify-between">
                                    <p class="post_col">{{$query->name}}</p>
                                    <div class="d-flex flex-self-center">
                                        <a href="{{ route('subcategory.fetch', ['subcategory' => $query->id]) }}" title="EDIT" class="no-decor flex-self-center mr-3">
                                            <span class="mif-open-book edit_btn"></span>
                                        </a>
                                        <a href="{{ route('subcategory.delete', ['subcategory' => $query->id]) }}" title="DELETE" class="no-decor flex-self-center con_del">
                                            <span class="mif-bin close_btn"></span>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
