<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class media extends Model
{
    protected $guarded = [];

    public function gallery(){
        return $this->hasMany(gallery::class);
    }

}
