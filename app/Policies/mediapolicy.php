<?php

namespace App\Policies;

use App\User;
use App\media;
use Illuminate\Auth\Access\HandlesAuthorization;

class mediapolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any media.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can view the media.
     *
     * @param  \App\User  $user
     * @param  \App\media  $media
     * @return mixed
     */
    public function view(User $user, media $media)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can create media.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array($user->role, [
           'admin'
        ]);
    }

    /**
     * Determine whether the user can update the media.
     *
     * @param  \App\User  $user
     * @param  \App\media  $media
     * @return mixed
     */
    public function update(User $user, media $media)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can delete the media.
     *
     * @param  \App\User  $user
     * @param  \App\media  $media
     * @return mixed
     */
    public function delete(User $user, media $media)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can restore the media.
     *
     * @param  \App\User  $user
     * @param  \App\media  $media
     * @return mixed
     */
    public function restore(User $user, media $media)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the media.
     *
     * @param  \App\User  $user
     * @param  \App\media  $media
     * @return mixed
     */
    public function forceDelete(User $user, media $media)
    {
        //
    }
}
