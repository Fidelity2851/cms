<?php

namespace App\Policies;

use App\User;
use App\faq;
use Illuminate\Auth\Access\HandlesAuthorization;

class faqpolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any faqs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can view the faq.
     *
     * @param  \App\User  $user
     * @param  \App\faq  $faq
     * @return mixed
     */
    public function view(User $user, faq $faq)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can create faqs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array($user->role, [
           'admin'
        ]);
    }

    /**
     * Determine whether the user can update the faq.
     *
     * @param  \App\User  $user
     * @param  \App\faq  $faq
     * @return mixed
     */
    public function update(User $user, faq $faq)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can delete the faq.
     *
     * @param  \App\User  $user
     * @param  \App\faq  $faq
     * @return mixed
     */
    public function delete(User $user, faq $faq)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can restore the faq.
     *
     * @param  \App\User  $user
     * @param  \App\faq  $faq
     * @return mixed
     */
    public function restore(User $user, faq $faq)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the faq.
     *
     * @param  \App\User  $user
     * @param  \App\faq  $faq
     * @return mixed
     */
    public function forceDelete(User $user, faq $faq)
    {
        //
    }
}
