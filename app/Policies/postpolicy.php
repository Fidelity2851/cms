<?php

namespace App\Policies;

use App\User;
use App\post;
use Illuminate\Auth\Access\HandlesAuthorization;

class postpolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any posts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return in_array($user->role, [
            'admin',
            'writter'
        ]);
    }

    /**
     * Determine whether the user can Approve OR Pend the post.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function approve(User $user)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\User  $user
     * @param  \App\post  $post
     * @return mixed
     */
    public function view(User $user, post $post)
    {
        if (($user->role == 'admin')){
            return true;
        }
        elseif (($user->role == 'writter')){
            return $user->id === $post->user_id;
        }
        //return $user->id === $post->user_id;

    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array($user->role, [
            'admin',
            'writter'
        ]);
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\User  $user
     * @param  \App\post  $post
     * @return mixed
     */
    public function update(User $user, post $post)
    {
        if (($user->role == 'admin')){
            return true;
        }
        elseif (($user->role == 'writter')){
            return $user->id === $post->user_id;
        }
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\User  $user
     * @param  \App\post  $post
     * @return mixed
     */
    public function delete(User $user, post $post)
    {
        if (($user->role == 'admin')){
            return true;
        }
        elseif (($user->role == 'writter')){
            return $user->id === $post->user_id;
        }
    }

    /**
     * Determine whether the user can restore the post.
     *
     * @param  \App\User  $user
     * @param  \App\post  $post
     * @return mixed
     */
    public function restore(User $user, post $post)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the post.
     *
     * @param  \App\User  $user
     * @param  \App\post  $post
     * @return mixed
     */
    public function forceDelete(User $user, post $post)
    {
        //
    }
}
