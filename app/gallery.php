<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gallery extends Model
{
    protected $guarded = [];

    public function media(){
        return $this->belongsTo(media::class);
    }
}
