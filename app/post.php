<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(user::class);
    }

    public function category(){
        return $this->belongsTo(category::class);
    }

    public function subcategory(){
        return $this->belongsTo(subcategory::class);
    }

    public function tag(){
        return $this->hasMany(tag::class);
    }
}
