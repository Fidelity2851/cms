<?php

namespace App\Http\Controllers;

use App\tag;
use Auth;
use App\User;
use App\category;
use App\post;
use App\subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\Console\Input\Input;

class postcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('viewAny', post::class);

        $act = 1;
        if (Gate::allows('isAdmin')) {

            $post = post::orderby('id', 'desc')->paginate(15);
            $post1 = category::all();
            $post2 = subcategory::all();

            return view('admin.post', ['query'=>$post, 'query1'=>$post1, 'query2'=>$post2, 'post' => $act]);
        }
        elseif (Gate::allows('isWritter')) {

            $post = post::where('user_id', Auth::User()->id)->paginate(15);
            $post1 = category::all();
            $post2 = subcategory::all();

            return view('admin.post', ['query'=>$post, 'query1'=>$post1, 'query2'=>$post2, 'post' => $act]);
        }
    }

    public function indexapprove(){
        $this->authorize('viewAny', post::class);

        $act = 1;
        if (Gate::allows('isAdmin')) {

            $post = post::where('status', '1')->orderby('id', 'desc')->paginate(15);
            $post1 = category::all();
            $post2 = subcategory::all();

            return view('admin.post', ['query'=>$post, 'query1'=>$post1, 'query2'=>$post2, 'post' => $act]);
        }
        elseif (Gate::allows('isWritter')) {

            $post = post::where('status', '1')->where('user_id', Auth::User()->id)->paginate(15);
            $post1 = category::all();
            $post2 = subcategory::all();

            return view('admin.post', ['query'=>$post, 'query1'=>$post1, 'query2'=>$post2, 'post' => $act]);
        }
    }

    public function indexpending(){
        $this->authorize('viewAny', post::class);

        $act = 1;
        if (Gate::allows('isAdmin')) {

            $post = post::where('status', '0')->orderby('id', 'desc')->paginate(15);
            $post1 = category::all();
            $post2 = subcategory::all();

            return view('admin.post', ['query'=>$post, 'query1'=>$post1, 'query2'=>$post2, 'post' => $act]);
        }
        elseif (Gate::allows('isWritter')) {

            $post = post::where('status', '0')->where('user_id', Auth::User()->id)->paginate(15);
            $post1 = category::all();
            $post2 = subcategory::all();

            return view('admin.post', ['query'=>$post, 'query1'=>$post1, 'query2'=>$post2, 'post' => $act]);
        }
    }

    public function store(){
        $this->authorize('create', post::class);

        \request()->validate([
            'title'=>'required',
            'slug'=>'',
            'image'=>'required|file|image',
            'category'=>'required',
            'subcategory'=>'required',
            'summary'=>'required',
            'tags'=>'required',
            'description'=>'required',
            'pub_date'=>'required',
        ]);


        $msg = "successfully";
        $post = new post();
        $post->title = \request('title');
        $post->slug = \request('slug');
        $post->image = \request('image')->store('uploads', 'public');
        $post->category_id = \request('category');
        $post->subcategory_id = \request('subcategory');
        $post->user_id = Auth::user()->id;
        $post->summary = \request('summary');
        $post->description = \request('description');

        //checking whether Admin OR Writter to assign STATUS
        if (Gate::allows('isAdmin')) {
            $post->status = 1;
        }
        elseif (Gate::allows('isWritter')) {
            $post->status = 0;
        }

        $post->pub_at = \request('pub_date');
        $post->save();

        $tags = \request('tags');
        if (!empty($tags)){
            if (strpos($tags, ',') !== false){
                $taglist = explode(',', $tags);

                foreach ($taglist as $tag){
                    $post->tag()->create([
                        'name' => $tag,
                    ]);
                }
            }
        }

        //image resizing
        $image = Image::make(public_path('storage/' . $post->image))->fit(200, 200, null, 'top');
        $image->save();

        return back()->with('msg', 'Created Successfully');
    }

    public function ajax_sub_cate($id){

        $querys = subcategory::where('category_id',$id)->get();

        return response()->json($querys);
    }

    public function approve(post $post){
        $this->authorize('approve', $post);

        post::findorfail($post->id)->update([
            'status' => 1 ,
        ]);

        return back()->with('msg', 'Approved Successfully');
    }

    public function pending(post $post){
        $this->authorize('approve', $post);

        post::findorfail($post->id)->update([
            'status' => 0 ,
        ]);

        return back()->with('msg', 'Pending Successfully');
    }

    public function show(post $post){
        $this->authorize('view', $post);

        $act = 1;

        $query = post::findorfail($post->id);
        if ($query->has('tag')){
            $tag = $query->tag->pluck('name')->toArray();
            $tags = implode(', ', $tag);
        }
        else{
            $tags = "";
        }
        $query1 = category::all();
        $query2 = subcategory::all();
        return view('admin.editpost', ['post'=>$act, 'query'=>$query, 'query1'=>$query1, 'query2'=>$query2, 'tag'=>$tags]);
    }

    public function update(post $post){
        $this->authorize('update', $post);

        $data = \request()->validate([
            'title'=>'required',
            'slug'=>'',
            'image'=>'file|image',
            'category'=>'required',
            'subcategory'=>'required',
            'summary'=>'required',
            'tags'=>'required',
            'description'=>'required',
            'pub_date'=>'required',
        ]);

        if (empty(\request('image'))){
            post::findorfail($post->id)
                ->update([
                    'title' => \request('title'),
                    'slug' => \request('slug'),
                    'category_id' => \request('category'),
                    'subcategory_id' => \request('subcategory'),
                    'summary' => \request('summary'),
                    'description' => \request('description'),
                    'pub_at' => \request('pub_date')
                ]);
            $tags = \request('tags');
            if (!empty($tags)){
                if (strpos($tags, ',') !== false){
                    $taglist = explode(',', $tags);
                    tag::where('post_id', $post->id)->delete();
                    foreach ($taglist as $tag){
                        tag::create([
                            'post_id'=>$post->id,
                            'name'=>$tag,
                        ]);
                    }
                }
            }
        }
        else{

            $post = post::findorfail($post->id);
            if ($post->image){
                Storage::delete('/public/' .$post->image);
            }
            $post->update([
                'title' => \request('title'),
                'slug' => \request('slug'),
                'image' => \request('image')->store('uploads', 'public'),
                'category_id' => \request('category'),
                'subcategory_id' => \request('subcategory'),
                'summary' => \request('summary'),
                'description' => \request('description'),
                'pub_at' => \request('pub_date')
            ]);
            $tags = \request('tags');
            if (!empty($tags)){
                if (strpos($tags, ',') !== false){
                    $taglist = explode(',', $tags);
                    tag::where('post_id', $post->id)->delete();
                    foreach ($taglist as $tag){
                        tag::create([
                            'post_id'=>$post->id,
                            'name'=>$tag,
                        ]);
                    }
                }
            }
            //image resizing
            $image = Image::make(public_path('storage/' . $post->image))->fit(200, 200, null, 'top');
            $image->save();
        }

        return back()->with('msg', 'Updated Successfully');
    }

    public function destroy(post $post){
        $this->authorize('delete', $post);

        $delect = post::findorfail($post->id);
        $delect->delete();

        return back()->with('msg', 'Delected Successfully');
    }

    public function destroymany(Request $request){
        \request()->validate([
            'checked'=>'',
        ]);
        $checked = $request->input('checked');
        post::whereIn('id', $checked)->delete();

        return back()->with('msg', 'Delected Successfully');
    }
}
