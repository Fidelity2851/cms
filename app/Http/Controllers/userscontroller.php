<?php

namespace App\Http\Controllers;

use Auth;
use App\post;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Gate;

class userscontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public  function index(){
        $this->authorize('viewAny', User::class);

        $act = 1;

        if (Gate::allows('isAdmin')) {

            $users = User::orderby('id', 'desc')->paginate(15);

            return view('admin.users', ['user'=>$act,'users'=>$users]);
        }
        elseif (Gate::allows('isWritter')) {

            $users = User::where('id', Auth::User()->id)->paginate(15);

            return view('admin.users', ['user'=>$act,'users'=>$users]);
        }

    }

    public function store(){
        $this->authorize('create', User::class);

        \request()->validate([
            'username'=>['required', 'string', 'max:255', 'unique:users'],
            'email'=>['required', 'string', 'email', 'max:255', 'unique:users'],
            'image'=>'required|file|image',
            'role'=>'required',
            'password'=>'same:comfirm_password',
            'comfirm_password'=>'required|same:password',
            'biograph'=>'required'
        ]);

        $user = new User();
        $user->username = \request('username');
        $user->email = \request('email');
        $user->role = \request('role');
        $user->status = 1;
        $user->image = \request('image')->store('uploads', 'public');
        $user->password = Hash::make(\request ('password'));
        $user->biograph = \request('biograph');
        $user->save();

        //image resizing
        $image = Image::make(public_path('storage/' . $user->image))->fit(150, 150, null, 'top');
        $image->save();

        return back()->with('msg', 'Created Successfully');
    }

    public function block(User $user){
        $this->authorize('delete', $user);

        User::findorfail($user->id)->update([
           'status' => 0,
        ]);

        return back()->with('msg', 'User blocked Successfully');
    }

    public function unblock(User $user){
        $this->authorize('delete', $user);

        User::findorfail($user->id)->update([
            'status' => 1,
        ]);

        return back()->with('msg', 'User Unblocked Successfully');
    }

    public function show(User $user){
        $this->authorize('view', $user);

        $act = 1;
        $query = User::findorfail($user->id);
        return view('admin.editusers', ['user'=>$act,'query'=>$query]);
    }

    public function update(User $user){
        $this->authorize('update', $user);

        \request()->validate([
            'username'=>'required',
            'email'=>'required',
            'image'=>'file|image',
            'role'=>'required',
            'password'=>'same:comfirm_password',
            'comfirm_password'=>'same:password',
            'biograph'=>'required'
        ]);

        if (empty(\request('image')) && empty(\request('password'))){
            $user = User::findorfail($user->id);
            $user->update([
                'username' => \request('username'),
                'email' => \request('email'),
                'role' => \request('role'),
                'biograph' => \request('biograph')
            ]);
        }
        elseif (empty(\request('image')) ){
            $user = User::findorfail($user->id);
                $user->update([
                    'username' => \request('username'),
                    'email' => \request('email'),
                    'role' => \request('role'),
                    'password' => Hash::make(\request ('password')),
                    'biograph' => \request('biograph')
                ]);
        }
        elseif (empty(\request('password'))){
            $user = User::findorfail($user->id);
            if ($user->image){
                Storage::delete('/public/' .$user->image);
            }
                $user->update([
                    'username' => \request('username'),
                    'email' => \request('email'),
                    'role' => \request('role'),
                    'image' => \request('image')->store('uploads', 'public'),
                    'biograph' => \request('biograph')
                ]);
            $image = Image::make(public_path('storage/' . $user->image))->fit(150, 150, null, 'top');
            $image->save();
        }
        else{
            $user = User::findorfail($user->id);
            if ($user->image){
                Storage::delete('/public/' .$user->image);
            }
                $user->update([
                    'username' => \request('username'),
                    'email' => \request('email'),
                    'role' => \request('role'),
                    'image' => \request('image')->store('uploads', 'public'),
                    'password' => Hash::make(\request ('password')),
                    'biograph' => \request('biograph')
                ]);
            $image = Image::make(public_path('storage/' . $user->image))->fit(150, 150, null, 'top');
            $image->save();
        }

        return back()->with('msg', 'Updated Successfully');
    }

    public function destroy(User $user){
        $this->authorize('delete', $user);

        $delete = User::findorfail($user->id);
        $delete->post()->delete();
        $delete->delete();

        return back()->with('msg', 'Delected Successfully');
    }

    public function destroymany(Request $request){
        \request()->validate([
            'checked'=>'required',
        ]);
        $checked = $request->input('checked');
        User::whereIn('id', $checked)->delete();
        post::whereIn('user_id', $checked)->delete();

        return back()->with('msg', 'Deleted Successfully');
    }
}
