<?php

namespace App\Http\Controllers;

use App\banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class bannercontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('viewAny', banner::class);

        $act = 1;
        $banner = banner::orderby('id', 'desc')->paginate(15);

        return view('admin.banner', ['ban'=>$act, 'banners'=>$banner]);
    }

    public  function store(){
        $this->authorize('create', banner::class);

        \request()->validate([
           'title'=>'required',
           'image'=>'required|file|image',
           'description'=>'required'
        ]);

        $banner = new banner();
        $banner->title = \request('title');
        $banner->image = \request('image')->store('uploads', 'public');
        $banner->description = \request('description');
        $banner->save();

        $image = Image::make(public_path('storage/' . $banner->image))->fit(300, 150, null, 'top');
        $image->save();

        return back()->with('msg', 'Created Successfully');
    }

    public function show(banner $banner){
        $this->authorize('view', $banner);

        $act = 1;
        $query = banner::findorfail($banner->id);
        return view('admin.editbanner', ['ban'=>$act, 'query'=>$query]);
    }

    public function update(banner $banner){
        $this->authorize('update', $banner);

        \request()->validate([
            'title'=>'required',
            'image'=>'file|image',
            'description'=>'required',
        ]);

        if (empty(\request('image'))){
            banner::findorfail($banner->id)
                ->update([
                    'title' => \request('title'),
                    'description' => \request('description')
                ]);
        }
        else{
            $banner = banner::findorfail($banner->id);
            if ($banner->image){
                Storage::delete('/public/' .$banner->image);
            }
                $banner->update([
                    'title' => \request('title'),
                    'image' => \request('image')->store('uploads', 'public'),
                    'description' => \request('description')
                ]);
            $image = Image::make(public_path('storage/' . $banner->image))->fit(300, 150, null, 'top');
            $image->save();
        }

        return back()->with('msg', 'Updated Successfully');
    }

    public function destroy(banner $banner){
        $this->authorize('delete', $banner);

        $delect = banner::findorfail($banner->id);
        $delect->delete();

        return back()->with('msg', 'Delected Successfully');
    }

    public function destroymany(Request $request){
        \request()->validate([
            'checked'=>'',
        ]);
        $checked = $request->input('checked');
        banner::whereIn('id', $checked)->delete();

        return back()->with('msg', 'Deleted Successfully');
    }
}
