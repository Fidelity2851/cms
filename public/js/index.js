$(document) .ready(function () {

    //Closing Success Message
    $('.msg_btn') .click(function () {
        $('.msg_con') .fadeOut();
    });

    //Getting subcategory on post AJAX
    $('select[name="category"]') .change(function () {
        //category id
        var cate_id = $(this).val();

        // Empty the sub_category dropdown
        $('.sub_cate').find('option').remove();

        $.ajax({
            url: "/posts/ajax/"+cate_id,
            type: 'GET',
            dataType: 'json',
            /*error: function(xhr, status, error) {
                alert(xhr.responseText);
                $('.feedback') .text(xhr.responseText);
            },*/
            success: function(querys){
                if (querys == ""){
                    $('.feedback') .text("No Subcategory found");
                }
                else {
                    $('.feedback') .html("<span class=\"feedback2\">Subcategory found</span>");
                }
                $.each( querys, function( key, value ) {
                    var data = "<option value="+value['id']+">" +value['name']+ "</option>";
                    //appending to Select
                    $('.sub_cate').append(data);
                });
            }
        });
    });


    //Selecting multiple row
    $('input[name="check_all"]') .click(function () {
        if ($('input[name="check_all"]') .is(':checked')){
            $('input[type="checkbox"]') .prop('checked', true);
        }
        else {
            $('input[type="checkbox"]') .prop('checked', false);
        }
    });

    $('.del_all_btn') .click(function (e) {
        var checker = $('input[name="checked[]"]:checked').length > 0;
        if (checker){
            var allow1 = confirm('Are you sure you want to DELETE');
            if (allow1){
                return true;
            }
            else {
                return false;
            }
        }
        else {
            alert('No Row Selected')
            e.preventDefault();
        }
    });


    //confirming delete
    $('.con_del') .click(function () {
        var allow = confirm('Are you sure you want to DELETE');
        if (allow){
            return true;
        }
        else {
            return false;
        }
    });

    //confirming block
    $('.blo_btn') .click(function () {
        var allow = confirm('Are you sure you want to BLOCK');
        if (allow){
            return true;
        }
        else {
            return false;
        }
    });

    //confirming Unblock
    $('.unblo_btn') .click(function () {
        var allow = confirm('Are you sure you want to UNBLOCK');
        if (allow){
            return true;
        }
        else {
            return false;
        }
    });


});
