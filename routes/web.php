<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/*Login & Home page Route*/
Route::get('/', 'headercontroller@index');
Route::get('/dashboard', 'headercontroller@dash')->name('dashboard');


/*Admin Category Routes*/
Route::get('/category', 'categorycontroller@index')->name('category.list');
Route::post('/create/category', 'categorycontroller@store')->name('category.store');
Route::post('/create/subcategory', 'categorycontroller@store2')->name('subcategory.store');
Route::get('/category/{category}', 'categorycontroller@show')->name('category.fetch');
Route::get('/subcategory/{subcategory}', 'categorycontroller@show2')->name('subcategory.fetch');
Route::post('update/category/{category}', 'categorycontroller@update')->name('category.update');
Route::post('update/subcategory/{subcategory}', 'categorycontroller@update2')->name('subcategory.update');
Route::get('delete/category/{category}', 'categorycontroller@destroy')->name('category.delete');
Route::post('delete/category', 'categorycontroller@destroymany')->name('category.deletemany');
Route::get('delete/subcategory/{subcategory}', 'categorycontroller@destroy2')->name('subcategory.delete');

/*Admin Post Routes*/
Route::get('/posts', 'postcontroller@index')->name('post.list');
Route::get('/posts/approve', 'postcontroller@indexapprove')->name('post.listapprove');
Route::get('/posts/pending', 'postcontroller@indexpending')->name('post.listpending');
Route::post('/create/post', 'postcontroller@store')->name('post.store');
Route::get('/posts/{post}', 'postcontroller@show')->name('post.fetch');
Route::get('/posts/ajax/{id}', 'postcontroller@ajax_sub_cate');
Route::post('/update/post/{post}', 'postcontroller@update')->name('post.update');
Route::get('/approve/posts/{post}', 'postcontroller@approve')->name('post.approve');
Route::get('/pending/posts/{post}', 'postcontroller@pending')->name('post.pending');
Route::get('/delete/posts/{post}', 'postcontroller@destroy')->name('post.delete');
Route::post('/delete/posts', 'postcontroller@destroymany')->name('post.deletemany');


/*Admin Banner Routes*/
Route::get('/banner', 'bannercontroller@index')->name('banner.list');
Route::post('/create/banner', 'bannercontroller@store')->name('banner.store');
Route::get('/banner/{banner}', 'bannercontroller@show')->name('banner.fetch');
Route::post('/update/banner/{banner}', 'bannercontroller@update')->name('banner.update');
Route::get('/delete/banner/{banner}', 'bannercontroller@destroy')->name('banner.delete');
Route::post('/delete/banner}', 'bannercontroller@destroymany')->name('banner.deletemany');


/*Admin Media Routes*/
Route::get('/media', 'mediacontroller@index')->name('media.list');
Route::post('/create/media', 'mediacontroller@store')->name('media.store');
Route::post('/create/gallery', 'mediacontroller@store2')->name('gallery.store');
Route::get('/media/{media}', 'mediacontroller@show')->name('media.fetch');
Route::post('/update/media/{media}', 'mediacontroller@update')->name('media.update');
Route::post('/update/gallery/{gallery}', 'mediacontroller@update2')->name('gallery.update');
Route::get('/delete/media/{media}', 'mediacontroller@destroy')->name('media.delete');
Route::post('/delete/media', 'mediacontroller@destroymany')->name('media.deletemany');
Route::get('/delete/gallery/{gallery}', 'mediacontroller@destroy2')->name('gallery.delete');


/*Admin Faq Routes*/
Route::get('/faq', 'faqcontroller@index')->name('faq.list');
Route::post('/create/faq', 'faqcontroller@store')->name('faq.store');
Route::get('/faq/{faq}', 'faqcontroller@show')->name('faq.fetch');
Route::post('/update/faq/{faq}', 'faqcontroller@update')->name('faq.update');
Route::get('/delete/faq/{faq}', 'faqcontroller@destroy')->name('faq.delete');
Route::post('/delete/faq', 'faqcontroller@destroymany')->name('faq.deletemany');


/*Admin Users Routes*/
Route::get('/users', 'userscontroller@index')->name('users.list');
Route::post('/create/users', 'userscontroller@store')->name('users.store');
Route::get('/users/{user}', 'userscontroller@show')->name('users.fetch');
Route::post('/update/user/{user}', 'userscontroller@update')->name('users.update');
Route::get('/block/users/{user}', 'userscontroller@block')->name('users.block');
Route::get('/unblock/users/{user}', 'userscontroller@unblock')->name('users.unblock');
Route::get('/delete/users/{user}', 'userscontroller@destroy')->name('users.delete');
Route::post('/delete/users', 'userscontroller@destroymany')->name('users.deletemany');


/*Admin Settings Routes*/
Route::get('/setting', 'settingscontroller@index')->name('setting.list');
Route::post('/create/setting', 'settingscontroller@update')->name('setting.update');

